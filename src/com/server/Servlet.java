package com.server;

import java.io.OutputStream;


public interface Servlet {
    void init() throws Exception;

    void service(byte[] buffer, OutputStream outputStream) throws  Exception;

}
