package com.server;

import java.io.OutputStream;


public class addServlet implements Servlet{
    @Override
    public void init() throws Exception {
    }
    @Override
    public void service(byte[] buffer, OutputStream out) throws Exception {
        String request = new String(buffer);
        //获的HTTP请求的第一行
        String firstLineOfRequest = request.substring(0, request.indexOf("\r\n"));
        //解析HTTP请求的第一行
        String[] parts = firstLineOfRequest.split(" ");
        //获得HTTP请求中的uri
        String uri = parts[1];
        //uri=“http://localhost:8080/add?a=1&b=1”
        //parameters = "a=1&b=1"
        String parameters = uri.substring(uri.indexOf("?") + 1,uri.length());
        //parts = {"a=1","b=1"}
        parts = parameters.split("&");
        String  a = parts[0].split("=")[1];
        String  b = parts[1].split("=")[1];

        int sum = Integer.parseInt(a) + Integer.parseInt(b);
        /*创建并发送HTTP响应*/
        //发送HTTP响应的第一行
        out.write("HTTP/1.1 200 OK\r\n".getBytes());
        //发送响应头
        out.write("Content-Type:text/html\r\n\r\n".getBytes());
        //发送响应正文
        String content = "<html><head><title>HelloWorld"
                + "</title></head><body>";
        content+="<h1>"+ sum + "</h1></body></html>";
        out.write(content.getBytes());
    }
}
