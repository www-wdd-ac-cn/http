package com.server;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;


public class HTTPServer {
    //存放servlet实例的缓存
    private static Map<String, Servlet> servletCache = new HashMap<String,Servlet>();

    public static void main(String[] args) {

        int port = 8080;
        ServerSocket serverSocket;

        try {
            serverSocket = new ServerSocket(port);
            System.out.println("服务器正在监听的端口：" + serverSocket.getLocalPort());

            while (true){
                try {
                    final Socket socket = serverSocket.accept();
                    System.out.println("建立了与客户的一个新的TCP连接，该客户的地址为：" + socket.getInetAddress() + ":" + socket.getPort()) ;
                    service(socket);//响应客户端请求
                } catch (Exception e) {
                    System.out.println("客户端请求资源不存在");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void service(Socket socket) throws Exception {
        InputStream inputStream = socket.getInputStream();
        //睡眠500毫秒，等待http请求
        Thread.sleep(500);
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        String request = new String(buffer);

        /*解析HTTP请求*/
        //获得HTTP请求的第一行
        int endIndex = request.indexOf("\r\n");
        if (endIndex == -1){
            endIndex = request.length();
        }
        String firstLineOfRequest = request.substring(0, endIndex);
        //解析Http请求的第一行
        String[] parts = firstLineOfRequest.split(" ");
        String uri = "";
        if (parts.length >=2){
            uri = parts[1];//获取uri
        }

        String servletName = null;
        servletName = uri.substring(uri.indexOf("/") +1,uri.indexOf("?"));
        //尝试从Servlet缓存中获取Servlet对象
        Servlet servlet = servletCache.get(servletName);
        /*如果Servlet缓存中不存在Servlet对象，就创建它，并把它放在Servlet缓存中*/
        if (servlet == null) {
            // 运行代码需要更改包路径
            servlet = (Servlet)Class.forName("com.server." + servletName + "Servlet").getDeclaredConstructor().newInstance();
            servlet.init();
            servletCache.put(servletName,servlet);
        }
        //调用Servlet的service（）方法
        servlet.service(buffer,socket.getOutputStream());
        Thread.sleep(1000);//睡眠一秒等待客户端接受HTTP相应结果
        socket.close();//关闭TCP连接
        return;

    }
}
